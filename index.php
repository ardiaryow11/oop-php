<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new animal("shaun");
echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new frog("Buduk");
echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded. "<br>"; 
echo $kodok -> jump() . "<br><br>";


$sunggokong = new ape("kerasakti");
echo "Name : " . $sunggokong->name . "<br>"; 
echo "Legs : " . $sunggokong->legs . "<br>"; 
echo "Cold Blooded : " . $sunggokong->cold_blooded . "<br>"; 
echo $sunggokong -> yell() . "<br><br>";

?>